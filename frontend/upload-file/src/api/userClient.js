import axios from 'axios';
import configData from "./../config.json";

const axiosClient = axios.create({
  baseURL: configData.BASE_URL,
  headers: {
    'Content-type': 'application/json',
  },
});

// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    console.log(error);
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosClient.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    return Promise.reject(error.response ? error.response.data : {});
  }
);

export default axiosClient;
