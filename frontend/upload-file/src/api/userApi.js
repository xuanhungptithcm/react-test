import axiosClient from './userClient';

const userApi = {
  get() {
    const url = '/profile';
    return axiosClient.get(url);
  },
  async create(formData) {
    const url = '/profile';
    const header = { 'Content-Type': 'multipart/form-data' };
    try {
      return axiosClient.post(url, formData, {
        headers: header,
      });
    } catch (error) {
      console.log(error);
    }
  },
  async update(formData) {
    const url = '/profile';
    const header = { 'Content-Type': 'multipart/form-data' };
    try {
      return axiosClient.post(url, formData, {
        headers: header,
      });
    } catch (error) {}
  },
};

export default userApi;
