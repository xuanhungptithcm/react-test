import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { Controller } from 'react-hook-form';

InputField.propTypes = {
  form: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};

function InputField(props) {
  const { form, name, label, disabled } = props;
  const {
    formState: { errors },
  } = form;
  console.log();
  const hasError = errors[name];
  return (
    <div>
      <Controller
        control={form.control}
        name={name}
        render={({ field }) => (
          <TextField
            margin="normal"
            disabled={disabled}
            label={label}
            fullWidth
            onChange={(e) => field.onChange(e.target.value)}
            variant="outlined"
            error={!!hasError}
            helperText={errors[name]?.message}
            value={form.getValues()[name] || ""}
          />
        )}
      ></Controller>
    </div>
  );
}

export default InputField;
