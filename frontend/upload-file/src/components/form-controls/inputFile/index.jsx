import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { imagePath } from '../../../helper/imageHelper';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Alert from '@material-ui/lab/Alert';
import configData from './../../../config.json';
import { makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(17),
    height: theme.spacing(17),
    marginBottom: theme.spacing(2),
  },
}));
InputFile.propTypes = {
  onFileChange: PropTypes.func.isRequired,
  user: PropTypes.object,
};

function InputFile(props) {
  const { form, name, onFileChange, disabled = false } = props;
  const classes = useStyles();
  const MAX_SIZE_FILE_IMAGE = 5 * 1024 * 1024; // 5MB
  const [isOverSize, setIsOverSize] = useState(false);
  const [filePath, setFilePath] = useState('');
  const handleFileChange = (event) => {
    setIsOverSize(false);
    const files = event.target.files;
    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    const size = files[0].size;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    setIsOverSize(size > MAX_SIZE_FILE_IMAGE);
    if (isOverSize) return;
    setFilePath(URL.createObjectURL(files[0]));
    onFileChange(files[0]);
  };
  return (
    <div>
      <Avatar
        alt="avatar"
        src={
          !!filePath
            ? filePath
            : form.getValues()[name]
            ? ` ${configData.BASE_URL}/${form.getValues()[name]}`
            : imagePath('images/user.png')
        }
        className={classes.large}
      />
      <input
        onChange={handleFileChange}
        accept="image/*"
        className={classes.input}
        id="contained-button-file"
        type="file"
        disabled={disabled}
      />
      <label htmlFor="contained-button-file">
        <Button
          disabled={disabled}
          startIcon={<CloudUploadIcon />}
          variant="contained"
          color="primary"
          component="span"
        >
          Select image
        </Button>
      </label>
      {isOverSize && <Alert severity="error">Image size over 5MB, please upload with new image!</Alert>}
    </div>
  );
}

export default InputFile;
