import React, { useEffect, useState } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import './style.css';
import { Route } from 'react-router';
import User from './user';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import { useDispatch } from 'react-redux';
import { setUserReducer } from '../../redux/userSlice';
import userApi from '../../api/userApi';
const useStyles = makeStyles((theme) => ({
  wrapper: {
    marginLeft: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));
function Profile(props) {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    async function fetchApi() {
      try {
        const userResult = await userApi.get();
        dispatch(setUserReducer(userResult));
      } catch (error) {
        
      }
      setLoading(false);
    }
    setLoading(true);
    fetchApi();
    return () => {};
  }, []);
  return (
    <div className={classes.wrapper}>
      <Container className="container" maxWidth="sm">
        <Route path="/" exact component={User}></Route>
        {loading && <div className="blur" ></div>}
      </Container>
      {loading && <CircularProgress size={45} className={classes.buttonProgress} />}
    </div>
  );
}

export default Profile;
