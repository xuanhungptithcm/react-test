import React, { useEffect, useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import InputField from '../../form-controls/InputField';
import { useForm } from 'react-hook-form';
import Button from '@material-ui/core/Button';
import './style.css';
import Alert from '@material-ui/lab/Alert';
import userApi from '../../../api/userApi';
import { useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import InputFile from '../../form-controls/inputFile';
const MODE = {
  VIEW: 'view',
  EDIT: 'edit',
  CREATE: 'create',
};
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
  large: {
    width: theme.spacing(17),
    height: theme.spacing(17),
  },
  wrapper: {
    marginLeft: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

function User(props) {
  const schema = yup.object().shape({
    name: yup.string().required('Please enter name'),
    email: yup.string().email('Email not valid, please try again').required('Please enter email'),
    website: yup.string().url(),
  });
  const [mode, setMode] = useState(MODE.VIEW);
  const userSelector = useSelector((state) => state.user);
  const [file, setFile] = useState(null);

  const classes = useStyles();
  const [isUpdateError, setIsCreateError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [messageError, setMessageError] = useState('');
  const form = useForm({
    defaultValues: {
      name: '',
      email: '',
      website: '',
    },
    resolver: yupResolver(schema),
  });
  const handleSubmit = async (values) => {
    if (!loading) {
      setIsCreateError(false);
      setMessageError('');
      const formData = new FormData();
      formData.append('avatar', file);
      formData.append('name', values.name);
      formData.append('email', values.email);
      formData.append('website', values.website);
      setLoading(true);
      try {
        let result;
        if (mode === MODE.EDIT) {
          result = await userApi.update(formData);
        }
        if (mode === MODE.CREATE) {
          result = await userApi.create(formData);
        }
        if (result.success) {
          setMode(MODE.VIEW);
          return;
        }
      } catch (error) {
        setIsCreateError(true);
        if (error && (error.message || error.data)) {
          let messageError = error.message || error.data;
          if (Array.isArray(messageError)) {
            messageError = messageError.join(', ');
          }
          setMessageError(messageError || '');
        }
      }
      setLoading(false);
    }
  };

  useEffect(() => {
    if (!userSelector) {
      setMode(MODE.CREATE);
      return;
    }
    setMode(MODE.VIEW);
    fillValueFrom(userSelector);
    return () => {};
  }, [userSelector]);

  const onFileChange = (file) => {
    setFile(file);
  };
  const getTitle = () => {
    switch (mode) {
      case MODE.CREATE: {
        return 'Create user';
      }
      case MODE.EDIT: {
        return 'Edit user';
      }
      default:
        return 'View user';
    }
  };
  const fillValueFrom = (user) => {
    user = user || {};
    form.setValue('name', user.name || '');
    form.setValue('email', user.email || '');
    form.setValue('website', user.website || '');
    form.setValue('avatar', user.avatar || '');
  };
  return (
    <div className={classes.root}>
      <h4 className="title">{getTitle()}</h4>
      {isUpdateError && <Alert severity="error">{messageError || 'Server Error'}</Alert>}
      <form onSubmit={form.handleSubmit(handleSubmit)}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <InputField
              className={classes.paper}
              name="name"
              label="Name"
              form={form}
              disabled={mode === MODE.VIEW || loading}
            ></InputField>
            <InputField
              className={classes.paper}
              name="email"
              label="Email"
              form={form}
              disabled={mode === MODE.VIEW || loading}
            ></InputField>
            <InputField
              className={classes.paper}
              name="website"
              label="Website"
              form={form}
              disabled={mode === MODE.VIEW || loading}
            ></InputField>
            <div className="input-file">
              <InputFile
                form={form}
                name="avatar"
                disabled={mode === MODE.VIEW || loading}
                onFileChange={onFileChange}
                user={userSelector}
              ></InputFile>
            </div>
          </Grid>
        </Grid>
        {mode === MODE.VIEW && (
          <Grid spacing={1} container direction="row" justify="flex-end" alignItems="flex-start">
            <Button
              type="button"
              variant="contained"
              size="medium"
              color="primary"
              className={classes.margin}
              disabled={!userSelector}
              onClick={() => {
                if (userSelector) {
                  setMode(MODE.EDIT);
                }
              }}
            >
              Edit
            </Button>
            <Button
              type="button"
              variant="contained"
              size="medium"
              color="primary"
              className={[classes.margin, classes.wrapper].join(' ')}
              onClick={() => {
                setMode(MODE.CREATE);
                fillValueFrom({});
              }}
            >
              Create
            </Button>
          </Grid>
        )}
        {mode !== MODE.VIEW && (
          <Grid container direction="row" justify="flex-end" alignItems="flex-start">
            <Button
              type="button"
              variant="contained"
              size="medium"
              color="default"
              className={classes.margin}
              onClick={() => {
                setMode(MODE.VIEW);
                fillValueFrom(userSelector);
              }}
            >
              Back
            </Button>
            <div className={classes.wrapper}>
              <Button
                disabled={loading}
                type="submit"
                variant="contained"
                size="medium"
                color="primary"
                className={classes.margin}
              >
                Submit
              </Button>
              {loading && <CircularProgress size={25} className={classes.buttonProgress} />}
            </div>
          </Grid>
        )}
      </form>
    </div>
  );
}

export default User;
