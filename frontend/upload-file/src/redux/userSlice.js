const { createSlice } = require('@reduxjs/toolkit');

const userSlice = createSlice({
  name: 'user',
  initialState: null,
  reducers: {
    setUserReducer(state, action) {
      console.log(action.payload);
      return {
        ...action.payload,
      };
    },
    getUser(state) {
      return state;
    },
  },
});

const { actions, reducer } = userSlice;
export const { setUserReducer, getUser } = actions;
export default reducer;
