export const imagePath = (path) => {
  return `/assets/${path}`;
};
