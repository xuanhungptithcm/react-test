# ReactJS coding challenge
This a repository for your React coding challenge. Please read the requirements, clone this repository to your computer, implement the coding challenge in **frontend** folder.

## Introduction
The idea of this challenge is implementing a frontend for user profile page. The user profile consists of the following fields:
- avatar
- username
- email
- website (optional)

User can switch between view and edit mode.

In view mode, user can see the avatar and other text information like username, email, website. In edit mode, user can upload a new avatar, new avatar should be appear immediately after user select a file from computer. username and email are required fields. website is an optional field. Please validate email and website before send data to backend API. When data is submitted, the form should switch back to view mode.

You can freely choose the design for your form and we expect that it should be styled carefully.

## Dependencies
These are the dependencis that we expect you to use in this challenge:
- react
- redux / redux-toolkit
- styled-components
- axios
- react-hook-form
- webpack

## Code standard
- Please use 2 spaces for indent.
- Code must be formatted.
- You can setup the project by yourself, or create-react-app, or any boilerplate tool.
- backend API url should be loaded from .env
- Make your components reusable as much as possible.

## Before you start
Please ask any question if you want to clarify the requirment for this coding challenge. Also please test the application carefully before send to us. Thanks and good luck!
