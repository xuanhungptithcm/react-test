import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import multer from "multer";
import { getUser, saveUser } from "./database.js";
import yup from "yup";
import fs from "fs";
import path from "path";

const __dirname = process.cwd();
const app = express();
app.use(cors());
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public");
  },
  filename: function (req, file, cb) {
    let extArray = file.mimetype.split("/");
    let extension = extArray[extArray.length - 1];
    cb(null, file.fieldname + "-" + Date.now() + "." + extension);
  },
});
const upload = multer({ storage });
app.use("/public", express.static(__dirname + "/public"));
app.use(bodyParser.json());

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log("Backend app is listening at port " + port);
});

app.get("/profile", async (req, res) => {
  const user = await getUser();
  return res.json(user).send();
});

app.post("/profile", upload.single("avatar"), async (req, res) => {
  const schema = yup.object().shape({
    name: yup.string().required(),
    email: yup.string().email().required(),
    website: yup.string().url(),
  });
  const user = req.body || {};
  const existingUser = await getUser();
  try {
    await schema.validate(user);
  } catch (errors) {
    return res.status(400).json({
      success: false,
      data: errors.errors,
    });
  }

  try {
    if (req.file) {
      if (existingUser.avatar) {
        const filepath = path.join(__dirname, existingUser.avatar);
        try {
          fs.unlinkSync(filepath);
        } catch (error) {}
      }
      user.avatar = path.join("public", req.file.filename);
    } else {
      user.avatar = existingUser.avatar;
    }
    await saveUser(user);
    res.send({
      success: true,
      data: user,
    });
  } catch (e) {
    console.log(e);
    res.status(500).json({
      success: false,
      message: "Unable to save user",
    });
  }
});
