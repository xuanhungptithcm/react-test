import fs from 'fs';

export async function saveUser(userData) {
  return fs.writeFileSync('db.json', JSON.stringify(userData));
}

export async function getUser() {
  try {
    const data = await fs.readFileSync('db.json');
    if (!data) return {};
    return JSON.parse(data);
  } catch (e) {
    return {};
  }
}